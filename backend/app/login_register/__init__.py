from flask import Blueprint

login_register = Blueprint('login_register', __name__)

from . import check_name, create_account, login_verify