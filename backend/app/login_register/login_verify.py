from flask import request
from . import login_register
from .. import db
from ..models import User

@login_register.route('/login_verify', methods = ['GET', 'POST'])
def login_verify():
    if request.method == 'POST':
        name = request.form.get('name')
        password = request.form.get('password')
        # 查询
        user_list = User.query.filter_by(name = name).all()
        if len(user_list) == 0:
            return 'Unknown name!'
        else :
            for user in user_list:
                # 验证密码
                if user.validate_password(password):
                    return 'Login success!'
                else :
                    return 'Wrong password!'
    return 'Use POST request!'  