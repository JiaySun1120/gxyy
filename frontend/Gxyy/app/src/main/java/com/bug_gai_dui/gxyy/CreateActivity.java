package com.bug_gai_dui.gxyy;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.amap.api.maps2d.model.LatLng;
import okhttp3.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class CreateActivity extends AppCompatActivity {

    private EditText txtDate;
    private EditText txtEndDate;
    private Button btnRoadSet;
    private Button btnConfirm;
    Calendar calendar=Calendar.getInstance(Locale.CHINA);

    String activityName;
    Integer activityDateYear;
    Integer activityDateMonth;
    Integer activityDateDay;
    Integer activityEndDateYear;
    Integer activityEndDateMonth;
    Integer activityEndDateDay;

    ArrayList<Double> latitudeList = new ArrayList<>();
    ArrayList<Double> longitudeList = new ArrayList<>();
    ArrayList<Integer> typeList = new ArrayList<>();
    String activityIntro;
    String activityAttention;

    boolean isDateSetted = false;
    boolean isEndDateSetted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        txtDate = (EditText) findViewById(R.id.txtDate);
        txtEndDate = (EditText)findViewById(R.id.txtEndDate);
        btnRoadSet = (Button) findViewById(R.id.roadSet);
        btnConfirm = (Button) findViewById(R.id.confirm_create);

        txtDate.setFocusable(false);
        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(CreateActivity.this, R.style.MyDatePickerDialogTheme, new DatePickerDialog.OnDateSetListener() {
                    // 绑定监听器(How the parent is notified that the date is set.)
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // 此处得到选择的时间，可以进行你想要的操作
                        activityDateYear = year;
                        activityDateMonth = monthOfYear + 1;
                        activityDateDay = dayOfMonth;
                        isDateSetted = true;
                        txtDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    }
                },
                        // 设置初始日期
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        txtEndDate.setFocusable(false);
        txtEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(CreateActivity.this, R.style.MyDatePickerDialogTheme, new DatePickerDialog.OnDateSetListener() {
                    // 绑定监听器(How the parent is notified that the date is set.)
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // 此处得到选择的时间，可以进行你想要的操作
                        activityEndDateYear = year;
                        activityEndDateMonth = monthOfYear + 1;
                        activityEndDateDay = dayOfMonth;
                        isEndDateSetted = true;
                        txtEndDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    }
                },
                        // 设置初始日期
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btnRoadSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CreateActivity.this,MapSetActivity.class);
                startActivity(intent);
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityName = ((EditText) findViewById(R.id.name_activity)).getText().toString().trim();
                activityIntro = ((EditText) findViewById(R.id.intro)).getText().toString().trim();
                activityAttention = ((EditText) findViewById(R.id.attention)).getText().toString().trim();
                if(activityName.length() == 0) {
                    Toast.makeText(CreateActivity.this,"活动名称不能为空",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!isDateSetted) {
                    Toast.makeText(CreateActivity.this,"未设置活动日期",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!isEndDateSetted) {
                    Toast.makeText(CreateActivity.this, "未设置截止日期", Toast.LENGTH_SHORT).show();
                    return;
                }
                confirm();
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        latitudeList = (ArrayList<Double>) intent.getSerializableExtra("lat");
        longitudeList = (ArrayList<Double>) intent.getSerializableExtra("lng");
        typeList = intent.getIntegerArrayListExtra("type");
    }

    public void confirm() {
        new Thread(() -> {
            OkHttpClient okHttpClient = new OkHttpClient();

            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                json.put("username", "wss"); //暂时先这样
                json.put("activityname",activityName);

                JSONObject dateJson = new JSONObject();
                dateJson.put("year",activityDateYear);
                dateJson.put("month",activityDateMonth);
                dateJson.put("day",activityDateDay);
                json.put("activitydate",dateJson);

                JSONObject endDateJson = new JSONObject();
                endDateJson.put("year",activityEndDateYear);
                endDateJson.put("month",activityEndDateMonth);
                endDateJson.put("day",activityEndDateDay);
                json.put("activitydeaddate",endDateJson);

                json.put("activityintro",activityIntro);
                json.put("activityattention",activityAttention);


                JSONObject keyPointListJson = new JSONObject();
                for(int i = 0; i < latitudeList.size(); i++)
                {
                    JSONObject keyPointJson = new JSONObject();
                    keyPointJson.put("latitude",latitudeList.get(i));
                    keyPointJson.put("longitude",longitudeList.get(i));
                    keyPointJson.put("type",typeList.get(i));
                    keyPointListJson.put("keypoint"+i,keyPointJson);
                }
                json.put("keypoint_list",keyPointListJson);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
            Request request = new Request.Builder().url("http://127.0.0.1:5000/create_activity") //创建活动接口
                    .post(requestBody)
                    .build();

            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(CreateActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    final String res = response.body().string();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (res.equals("Done!")) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(CreateActivity.this, "创建成功", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }).start();
    }
}